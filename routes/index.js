var express = require('express');
var router = express.Router();
var db = require('../queries');

// To add a new route, follow this example
// Where db.ctrl is a function declared in ../queries.js
//
// router.get('/api/x', db.ctrl);
// router.get('/api/x/:id', db.ctrl);
// router.post('/api/x', db.ctrl);
// router.put('/api/x/:id', db.ctrl);
// router.delete('/api/x/:id', db.ctrl);


// members
router.post('/api/member', db.createMember);
router.get('/api/member/:id', db.getMemberById);

router.get('/api/member/:id/selection', db.getAllMembers);
router.get('/api/member/ready', db.isProfileReady);

router.get('/api/picture', db.getAllPictures);

router.post('/api/like', db.likeSomeone);

// application
router.get('/', function (req, res) {
    res.render('index', {title: 'Vision api'});
});

module.exports = router;
