const Vision = require('@google-cloud/vision');
const Q = require('q');
const promise = require('bluebird');
const request = require('request');
const conf = require('./conf');

const options = {
  promiseLib: promise
};

const pgp = require('pg-promise')(options);
const db = pgp(conf.db);

const vision = Vision({
  projectId: conf.visionAppId,
  keyFilename: './vision-key.json'
});

/**
 * get members
 */
function getAllMembers(req, res, next) {

    db.any('select * from member where id = ${id}', req.params)
    .then(users => {

        users[0].reversKvk = users[0].kvk.split('').reverse().join('')

    let sql= 'select u.id, u.username, u.profile_picture_url, sum(weight) as final_weight, array_agg(t2.label || \'|\' ||  coalesce(emo.emoji, \' \')) as labels \n' +
        'from member u \n' +
        'inner join picture p2 on u.id = p2.member_id \n' +
        'inner join tag t2 on p2.id = t2.picture_id \n' +
        'inner join ( \n' +
        '    select t.label, count(*) as weight \n' +
        '    from picture p \n' +
        '    inner join tag t on p.id = t.picture_id \n' +
        '    where p.member_id = ${id} \n' +
        '    group by t.label \n' +
        ') sub on sub.label = t2.label \n' +
        'left join emoji_text emo on emo.word = t2.label \n' +
        'where u.id not in ( \n' +
        '    select member_to \n' +
        '    from match \n' +
        '    where member_from = ${id} \n' +
        ') \n' +
        'and u.kvk = ${reversKvk} \n' +
        'group by u.id, u.username, u.profile_picture_url \n' +
        'order by final_weight ASC'


        db.any(sql, users[0])
        .then(function (data) {
          let finalSelection = [];
          data.forEach(dat => {
            let words = [];
            let datFin = {id: dat.id, username: dat.username, profile_picture_url: dat.profile_picture_url, final_weight: dat.final_weight, labels: []};
            dat.labels.forEach(lab => {
              let finLab = [];
              let oneLabel = lab.split('|');
              if (words.indexOf(oneLabel[0]) === -1) {
                words.push(oneLabel[0]);
                finLab = {label: oneLabel[0], emoji: oneLabel[1]};
                datFin.labels.push(finLab);
              }
            });
            finalSelection.push(datFin);
          });

          res.status(200)
            .json({
              status: 'success',
              data: finalSelection,
              message: 'Retrieved ALL members'
            });
        })
        .catch(function (err) {
          return next(err);
        });

    })
}

/**
 * get member by id with pics
 */
function getMemberById(req, res, next) {
  db.one('select * from member where id = ${id}', {id: req.params.id})
  .then(function (data) {
    db.any('select p.id, p.url, array_agg(t.label || \'|\' || coalesce(e.emoji, \' \')) as labels from picture p left join tag t on t.picture_id = p.id left join emoji_text e on e.word = t.label ' +
     'where member_id = ${member_id} GROUP BY p.id, p.url ', {member_id: data.id})
    .then(function(pictures) {
      let fin = [];

      pictures.forEach(pic => {
        let pictureFinal = {id: pic.id, url: pic.url, labels: []};
        let words = [];
        pic.labels.forEach(lab => {
          let oneLabel = lab.split('|');
          if (words.indexOf(oneLabel[0]) === -1) {
            words.push(oneLabel[0]);
            pictureFinal.labels.push({label: oneLabel[0], emoji: oneLabel[1]});
          }
        });
        fin.push(pictureFinal);
      });

      data.pictures = fin;
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved member'
        });
    }).catch(function(err) {
      console.log(err);
      return next(err);
    });
  })
  .catch(function (err) {
    return next(err);
  });
};

function isProfileReady(req, res, next) {
  res.json({ready: false});
};

function registerInDb(userId, finalPictures) {
  finalPictures.forEach(item => {
    db.one('insert into picture(url, member_id) values(${url}, ${userId}) RETURNING id', {url: item.pic, userId}).then(res => {
      item.labels.forEach(label => {
        db.one('insert into tag(picture_id, label) values(${pictureId}, ${label}) RETURNING id', {pictureId: res.id, label}).then(data => {
        });
      });
    });
  });
};

/**
 * get labels from a list of pictures
 * maybe the ugliest function evr coded
 */
function getLabelsFromVision(picturesUris, userId, res) {
  let promises = [];
  let labels = [];

  /* TODO: remove when ready */
  // picturesUris = [
  //   'https://scontent-cdg2-1.cdninstagram.com/t51.2885-19/s150x150/20759280_110032399698131_4765816138229612544_a.jpg',
  //   'https://scontent-cdg2-1.cdninstagram.com/t51.2885-15/e35/20582878_1728429987458050_3225270631113162752_n.jpg'
  // ];

  picturesUris.forEach((item, index) => {
    let promise = vision.labelDetection({source: {imageUri: item}}).then((results) => {
      return {img: item, labels: results[0].labelAnnotations};
    });
    promises.push(promise);
  });

  Q.all(promises).then(data => {
    let final = [];
    data.forEach(d => {
      let elem = {
        pic: d.img,
        labels: []
      };

      d.labels.forEach(lab => {
        elem.labels.push(lab.description);
      });
      final.push(elem);
    });
    res.status(200)
      .json({
        status: 'success',
        data: userId,
        message: 'Inserted one member'
      });
    registerInDb(userId, final);
    data.forEach(da => {
      da.labels.forEach(dala => {
        console.log('New label found: ' + dala.description);
        var start = Date.now(),
            now = start;
        while (now - start < 10) {
          now = Date.now();
        }
      });
    });
  });
}

/**
 * create member
 */
function createMember(req, res, next) {
  //first, check if the user exists
  db.any('select * from member where username = ${username}', {username: req.body.username})
  .then(users => {
    if (users.length > 0) {
      res.json({
        status: 'success',
        data: users[0].id,
        message: 'Member already exists'
      });
    } else {
      db.one('insert into member(username, birthdate, profile_picture_url, instagram_token, kvk)' +
          'values(${username}, ${birthdate}, ${profile_picture_url}, ${instagram_token}, ${kvk}) RETURNING id',
        req.body)
        .then(function (result) {
          registration(req.body.instagram_token, result.id, res);
        })
        .catch(function (err) {
          return next(err);
        });
    }


  });
};

function registration(instagramToken, userId, res) {
  let url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' + instagramToken;
  console.log('looking for instagram pics for token : ' + instagramToken + ' (userId = ' + userId + ')');

  sleeper(url);
  function maxValue(array, number) {
      let mostLikedArray = [];
      for (i = 0; i < number; i++) {
          let mostLikedCounter = Math.max.apply(Math,array.map(function(o){return o.likes.count;}));
          let mostLiked = array.find(function(o){ return o.likes.count == mostLikedCounter; });
          mostLikedArray.push(mostLiked);
          array.splice(array.indexOf(mostLiked), 1);
      }
      return mostLikedArray;
  }


  async function sleeper(url) {
      let pictures = [];

      let promise = new Promise(resolve => request(url, function (error, response, body) {

          let media = JSON.parse(body).data;
          media = maxValue(media, 10);
          media.forEach(function(med){
              pictures.push(med.images.standard_resolution.url);
          });
          // final call to google vision api
          getLabelsFromVision(pictures, userId, res);
      }));

      await promise
  }

}

function getAllPictures(req, res, next) {
  db.any('select * from picture')
  .then(function (data) {
    res.status(200)
      .json({
        status: 'success',
        data: data,
        message: 'Retrieved ALL members'
      });
  })
  .catch(function (err) {
    return next(err);
  });
};

function likeSomeone(req, res, next) {
  if (!req.body.member_from || !req.body.member_to || !req.body.kind) {
    res.json({error: 'need all arguments'});
    return;
  }
  db.any('insert into match(member_from, member_to, kind) values(${member_from}, ${member_to}, ${kind}) RETURNING id', req.body)
  .then(result => {
    /* reverse select to catch any match */
    db.any('select * from match where member_to = ${me} and member_from = ${other} and kind != \'dislike\'', {me: req.body.member_from, other: req.body.member_to})
    .then(results => {
      /* returns ok or match */
      res.status(200).json({status: 'success', data: results.length > 0 && req.body.kind !== 'dislike' ? 'match' : 'ok', message: req.body.kind + ' sent'});
    });
  }).catch(err => {
    return next(err);
  });
};

module.exports = {
  getAllMembers: getAllMembers,
  createMember: createMember,
  isProfileReady: isProfileReady,
  getAllPictures: getAllPictures,
  likeSomeone: likeSomeone,
  getMemberById: getMemberById
};
