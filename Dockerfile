FROM node:latest
RUN mkdir -p /app
WORKDIR /app

COPY . /app/

RUN cp conf.js.example conf.js && rm -rf node_modules && npm install

EXPOSE 3000

CMD [ "npm", "start" ]
