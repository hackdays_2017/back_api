install:
	docker compose up
up:
	docker run --name hackdays_api -p 49160:8080 -d hackdays_2017
stop:
	docker stop hackdays_api
cli:
	docker exec -it hackdays_api /bin/bash
logs:
	docker logs hackdays_api
