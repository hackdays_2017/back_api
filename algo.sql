select u.id, sum(weight) as final_weight, array_agg(distinct (t2.label, emo.emoji))
from member u
inner join picture p2 on u.id = p2.member_id
inner join tag t2 on p2.id = t2.picture_id
inner join (
    select t.label, count(*) as weight
    from picture p
    inner join tag t on p.id = t.picture_id
    where p.member_id = :LOGGED_MEMBER_ID
    group by t.label
) sub on sub.label = t2.label
left join emoji_text emo on emo.word = t2.label
where u.id not in (
    select member_to
    from match
    where member_from = :LOGGED_MEMBER_ID
)
and u.kvk = :REVERSE_KVK
group by u.id
order by final_weight DESC;
