#Vision rest api

1. Install dependencies - `npm install`
1. Run - `psql -U psqlUsername -f docker-images/postgres/psql_dump.sql`
1. Copy the conf locally - `cp conf.js.example conf.js` and edit it
1. Add the vision api key, under the name `vision-key.json`
1. Run the development server - `npm start`
